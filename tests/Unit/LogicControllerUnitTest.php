<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Http\Controllers\LogicController;
class LogicControllerUnitTest extends TestCase
{
    public function test_add_two_numbers()
    {
        $logicController = new LogicController;
        $number1 = 5;
        $number2 = 5;
        $this->assertEquals(10, $logicController->addTwoNumbers(5,5));
    }

    public function test_subtract_two_numbers()
    {
        $logicController = new LogicController;
        $number1 = 5;
        $number2 = 5;
        $this->assertEquals(0, $logicController->subtractTwoNumbers(5,5));
    }
}
