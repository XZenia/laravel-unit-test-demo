<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Patient;

class PatientControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_store_function()
    {
        $patient = new Patient();
        $patient->full_name = $this->faker->name;
        $patient->age = $this->faker->numberBetween(1, 100);
        $patient->address = $this->faker->address;
        $patient->height = $this->faker->numberBetween(100, 300);
        $patient->weight = $this->faker->numberBetween(30, 300);
        $patient->notes = $this->faker->sentence;
        $patient->status = $this->faker->numberBetween(1,3);

        $this->post(route('patients.store'), [
          'full_name' => $patient->full_name,
          'age' => $patient->age,
          'address' => $patient->address,
          'height'  => $patient->height,
          'weight'  => $patient->weight,
          'notes'   => $patient->notes,
          'status'  => $patient->status
        ])
        ->assertJson([
            'full_name' => $patient->full_name,
            'age' => $patient->age,
            'address' => $patient->address,
            'height'  => $patient->height,
            'weight'  => $patient->weight,
            'notes'   => $patient->notes,
            'status'  => $patient->status
        ]);
    }

    public function delete_function_test()
    {
        $id = 1;
        $this->delete(route('patients.delete'), $id)
              ->assertStatus(204);
    }
}
