<?php

use Illuminate\Database\Seeder;

class PatientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker\Factory::create();

      for ($counter = 0; $counter < 20; $counter++)
      {
          DB::table('patients')->insert([
              'full_name'   => $faker->name,
              'age'         => $faker->numberBetween(1, 100),
              'address'     => $faker->address,
              'height'      => $faker->numberBetween(100, 300),
              'weight'      => $faker->numberBetween(30, 300),
              'notes'       => $faker->sentence,
              'status'      => $faker->numberBetween(1,3)
          ]);
      }
    }
}
