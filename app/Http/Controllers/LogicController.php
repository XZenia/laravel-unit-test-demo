<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LogicController extends Controller
{
    public function addTwoNumbers($number1, $number2)
    {
        return $number1 + $number2;
    }

    public function subtractTwoNumbers($number1, $number2)
    {
        return $number1 - $number2;
    }
}
