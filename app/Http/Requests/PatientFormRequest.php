<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatientFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name'     => ['required'],
            'age'           => ['required', 'integer'],
            'address'       => ['required'],
            'height'        => ['required'],
            'weight'        => ['required'],
            'notes'         => ['string'],
            'status'        => ['required', 'integer']
        ];
    }
}
