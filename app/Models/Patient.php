<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $fillable = [
        'full_name',
        'age',
        'address',
        'height',
        'weight',
        'notes',
        'status'
    ];
}
